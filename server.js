// API Back TechU Practitioner
// Fidel Villalobos Martinez   2018-11-16


var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

// Se agrega excepiciones de seguridad para poder hacer metodos post desde navegador
// Se configura el acceso seguro para MLab
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// A todas las peticiones se le agrega el header
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Se agrega la dependencie a libreria request-json (nos ayudara a generar request a aPIS con respuesta JSON)
var requestjson = require('request-json');

var path = require('path');

// Soporte para guardar y comparar passwords con hash
const bcrypt = require('bcrypt');
const saltRounds = 10;

// Soporte para guardar sesiones de la API
var session = require('express-session');
app.use(session({
  secret: 'techu-practitioner-security',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true, maxAge: 60000*6 }
}));

var sess;

// Variable contador de ID de cuenta
var idCounter = 0;

// Var APIKEY mLab
var apiKeyValue = "apiKey=DjMnkFPl3anX6efNEI9FdIEmIZtRZ0jl";

// Se adiciona la url de la API donde esta almacenada nuestro MongoDB
var urlMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3mb79567/collections/movimientos?" + apiKeyValue;

// Se genera el cliente con la API request-json y la URL de la API a consumir
var clienteMLab = requestjson.createClient(urlMovimientos);

// Cambios para acceder a lista de usuarios (clientes)
// url de la API de usuarios en MongoDB mLab
var urlUsuarios = "https://api.mlab.com/api/1/databases/bdbanca3mb79567/collections/usuarios?" + apiKeyValue;
// Cliente API request-json con url de API
var clienteMLabusuarios = requestjson.createClient(urlUsuarios);

// url de tabla auxiliar del foliador
var urlAux = "https://api.mlab.com/api/1/databases/bdbanca3mb79567/collections/auxfoliador?" + apiKeyValue;

// url de la tabla de operacines del sistema
var urlOperaciones = "https://api.mlab.com/api/1/databases/bdbanca3mb79567/collections/operaciones?" + apiKeyValue;


app.listen(port);
console.log('todo list RESTful API server started on FDL: ' + port);


//----------------------------------------------------------------------------------
// Añadimos las consultas GET NODE para la API Mongo DB (GET)
app.get("/movimientos", function (req, res) {
  // Se genera el cliente con la API request-json y la URL de la API a consumir
  //var clienteMLab = requestjson.createClient(urlMovimientos);

  // Se hace el GET en el cliente.
  clienteMLab.get('', function (err, resM, body) {
    if (err) {
      console.log(err);
      alert(err);
    } else {
      //Envia la peticion al servidor
      res.send(body);
    }
  });

});


//----------------------------------------------------------------------------------
// Se define la peticion app POST  para insertar en NODE
app.post("/movimientos", function (req, res) {
  //
  clienteMLab.post('', req.body, function (err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      res.send(body);
    }
  }
  );

});


//----------------------------------------------------------------------------------
// Se define la peticion app get y redirige a directorio raiz /index.html
app.get('/', function (req, res) {
  console.log(req);
  res.sendFile(path.join(__dirname, "index.html"));
}
);


var validaSession = (req, res, next) => {
  console.log(" VALIDAR SERSSION : ");
  var session = req.session;
  var headers = req.headers;
  console.log(session.id);
  console.log(headers);
  console.log(headers.authorization);

  console.log("Fidel res:", req.sessionStore);

  if (!headers.authorization) {
    res.status(401).send("No cuenta con las credenciales suficientes");
  } else {

    req.sessionStore.get(headers.authorization, function (error, sesstoken) {

      if (error) {
        res.status(401).send("Credenciales desactualizadas");
      }
      else {
        console.log("Session id", sesstoken);
        if (sesstoken) {
          next();
        } else {
          res.status(401).send("Credenciales desactualizadas");
        }
      }

    });

  }
};


//Definiendo la estructura del API RESTful /clientes  y nos trae un JSON con clientes
app.get('/clientes/', function (req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
  var clientes =
    [{ idCliente: 1, nombre: "Fidel", apellido: "Villalobos" }, { msg: "Este es un test" }];

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(clientes));
});

//Definiendo la estructura del API RESTful Clientes pero se buscan con un ID
app.get('/clientes/:idCliente', function (req, res) {
  res.send("TEST: GET: Aqui tiene el cliente numero: " + req.params.idCliente);
}
);

app.post('/', function (req, res) {
  res.send("Su petición POST ha sido recibida y actualizada");
}
);

app.put('/', function (req, res) {
  res.send("Su petición PUT ha sido recibida");
}
);


app.delete('/', function (req, res) {
  res.send("Su petición DELETE ha sido recibida");
}
);


//--------------------------------------------
// API para consulta de usuarios 
app.get('/usuarios/', validaSession, function (req, res) {

  // Se hace el GET en el cliente de usuarios
  clienteMLabusuarios.get('', function (err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      //Envia la peticion al servidor
      res.send(body);
    }
  });

});

// Consulta el usuario si existe.
app.get('/get_usuarios_nombre/:idCliente', function (req, res) {
  var cliente = parseInt(req.params.idCliente);
  console.log(cliente + " is NAN: " + isNaN(cliente));
  if (!isNaN(cliente)) {
    var query = '&q={"idCliente":' + req.params.idCliente + '}&f={"idCliente":1,"nombre":1}&fo=true';

    console.log(" Query = " + query);

    var clienteMLabusuarios = requestjson.createClient(urlUsuarios + query);

    clienteMLabusuarios.get('', function (err, resM, body) {
      if (err) {
        console.log(err);
        res.status(400).send(body);
      } else {
        res.send(body);
        console.log("Res body: " + body);
      }
    });

    console.log(clienteMLabusuarios);
  }
  else {
    res.statusCode = 400;
    res.send("Su petición GET: cliente " + req.params.idCliente + " es Erronea." + "status code " + res.statusCode);
  }
});


// Consulta de usuarios por idCliente
app.get('/usuarios/:idCliente', validaSession, function (req, res) {
  //res.send("GET: Aqui tiene el cliente numero: " + req.params.idCliente);
  console.log("Cliente  a obtener : " + req.params.idCliente + " typeOf " + typeof (parseInt(req.params.idCliente)));
  // Armar el cliente
  var cliente = parseInt(req.params.idCliente);
  console.log(cliente + " is NAN: " + isNaN(cliente));

  if (!isNaN(cliente)) {
    var query = '&q={"idCliente":' + req.params.idCliente + "}&fo=true";

    console.log(" Query = " + query);

    var clienteMLabUsuariosId = requestjson.createClient(urlUsuarios + query);

    clienteMLabUsuariosId.get('', function (err, resM, body) {
      if (err) {
        console.log(err);
        alert(err);
      } else {
        res.send(body);
        console.log("Res body: " + body);

      }
    });

    console.log(clienteMLabUsuariosId);
  }
  else {
    res.statusCode = 400;
    res.send("Su petición GET: cliente " + req.params.idCliente + " es Erronea." + "status code " + res.statusCode);
  }
});


// POST  (Alta de un cliente)
app.post('/usuarios', function (req, res) {
  console.log("----- Peticion POST  recibida: alta de clientes");

  var validaUser = validarUsuarioInput(req.body);
  if (validaUser[0]) {
    console.log("validando usuario.", req.body);

    delete req.body.confirmPassword;
    req.body["idCliente"] = 0;

    // Asignar el nuevo ID del usuario
    var claveFoliador = "foliador_cliente_id";
    // Accedemos ala tabla auxiliar de contadores para obtener el numero de cliente a ingresar
    var id = claveFoliador;
    var query = '&q={"_id": "' + id + '"}&fo=true';
    var clienteMLab = requestjson.createClient(urlAux + query);
    var registro;
    console.log(clienteMLab);
    clienteMLab.get('', function (err, resM, body) {
      if (err) {
        console.log(err);
      } else {
        registro = body;
        console.log("Res body: ", registro._id, registro.value);
        registro.value = parseInt(registro.value) + 1;
        req.body["idCliente"] = registro.value;
        updateRegistroFoliadorId(registro);
        req.body["fechaAlta"] = new Date();

        // Cifrar el password a guardar
        var hash = bcrypt.hashSync(req.body["password"], saltRounds);
        req.body["password"] = hash;

        console.log("req.body a enviar ", req.body);

        // Finalmente hacer POST al cliente
        clienteMLabusuarios.post('', req.body, function (err, resM, body) {
          if (err) {
            console.log(err);
          } else {
            //
            console.log("Insercion correcta. New body es: ", body);
            // Eliminar campos 
            delete body.password;
            res.send(body);
          }
        });

      }
    });

  } else {
    res.statusCode = 400;
    res.send("Su petición POST incorrecta. El objeto body [ " + req.body + " ] " + "Status code " + res.statusCode
      + "<br> Error: " + validaUser[1]);
  }
});

function validarUsuarioInput(bodyUserInput) {
  var result = true;
  var message = "";

  console.log("validando entradas");
  var user = JSON.parse(JSON.stringify(bodyUserInput));
  console.log(user);

  // Validando que tiene todo correcto
  if (!user.hasOwnProperty("nombre")) {
    result = false;
    message += "Input user no tiene campo 'nombre' definido<br>";
  }
  else if (!user.nombre || user.nombre.length === 0) {
    result = false;
    message += "Campo 'nombre' no informado o vacio";
  }


  // Validando que tiene todo correcto
  if (!user.hasOwnProperty("apellido")) {
    result = false;
    message += "Input user no tiene campo 'apellido' definido<br>";
  }

  // Validando que tiene todo correcto
  if (!user.hasOwnProperty("email")) {
    result = false;
    message += "Input user no tiene campo 'email' definido<br>";
  }
  else if (!user.email || user.email.length === 0) {
    result = false;
    message += "Campo 'email' no informado o vacio";
  }

  // Validando que tiene todo correcto
  if (!user.hasOwnProperty("password")) {
    result = false;
    message += "Input user no tiene campo 'password' definido<br>";
  }
  else if (!user.password || user.password.length === 0) {
    result = false;
    message += "Campo 'password' no informado o vacio";
  }
  return [result, message];
}

// API para las cuentas de un cliente.
// Obtiene las cuentas ligadas a un cliente
app.get('/cuentas/:idCliente', validaSession, function (req, res) {

  console.log("--------- Su petición GET ha sido recibida. Obtener los movimientos del cliente: " + req.params.idCliente);

  //console.log("Cliente  a obtener : " + req.params.idCliente + " typeOf " + typeof (parseInt(req.params.idCliente)));
  // Armar el cliente
  var cliente = parseInt(req.params.idCliente);

  if (!isNaN(cliente)) {
    var query = '&q={"id_cliente":' + cliente + "}";

    console.log(" Query = " + query);

    var clienteMLab = requestjson.createClient(urlMovimientos + query);

    clienteMLab.get('', function (err, resM, body) {
      if (err) {
        //console.log(err);
        res.statusCode = 400;
        res.send(body);
      } else {
        res.send(body);
        console.log("Res body: " + body);

      }
    });

    console.log(clienteMLab);
  }
  else {
    res.statusCode = 400;
    res.send("Su petición GET: cliente " + req.params.idCliente + " es Erronea." + "status code " + res.statusCode);

  }

});

// Obtiene los movimientos de la cuenta del cliente indicado por idCuenta
app.get('/movimientos/:idCuenta', validaSession, function (req, res) {
  var idCuenta = parseInt(req.params.idCuenta);
  console.log("----> PETICION GET: cuenta " + idCuenta);
  if (!isNaN(idCuenta)) {
    var query = '&q={"_id":' + idCuenta + '}&fo=true';
    console.log(" Query = " + query);

    var clienteMLab = requestjson.createClient(urlMovimientos + query);
    //console.log(clienteMLab);

    clienteMLab.get('', function (err, resM, body) {
      if (err) {
        //console.log(err);
        res.statusCode = 400;
        res.send(body);
      } else {
        res.send(body);
        console.log("Res body: " + body);
      }
    });
  }
});



// API. Update para generar el alta de un nuevo movimiento sobre la cuenta.
app.put('/movimientos/:idCuenta', validaSession, function (req, res) {
  console.log("---------Su petición PUT ha sido recibida. Actualizar el numero de cuenta: " + req.params.idCuenta);

  var idCuenta = parseInt(req.params.idCuenta);

  if (!isNaN(idCuenta)) {
    // Asignar el nuero de folio para la operacion actual.
    // Asignar el nuevo ID del usuario
    var claveFoliador = "foliador_operacion_id";
    // Accedemos ala tabla auxiliar de contadores para obtener el numero de folio a actualizar
    var id = claveFoliador;
    var query = '&q={"_id": "' + id + '"}&fo=true';
    var clienteMLab = requestjson.createClient(urlAux + query);
    var registro;
    //console.log(clienteMLab);
    clienteMLab.get('', function (err, resM, body) {
      if (err) {
        console.log(err);
      } else {
        registro = body;
        console.log("Res body: ", registro._id, registro.value);
        registro.value = parseInt(registro.value) + 1;
        movimiento = req.body["$push"];
        console.log(movimiento);
        movimiento.listado.id = registro.value;
        movimiento.listado.importe = parseFloat(movimiento.listado.importe);
        movimiento.listado.fecha = new Date();
        console.log("after", movimiento);
        updateRegistroFoliadorId(registro);

        console.log("req.body a enviar ", req.body);

        // Finalmente hacer PUT de la operacion
        var query = '&q={"_id":' + idCuenta + "}";
        console.log(req.body);
        console.log(" Query = " + query);
        var clienteMLabMovs = requestjson.createClient(urlMovimientos + query);
        clienteMLabMovs.put('', req.body, function (err, resM, body) {
          if (err) {
            console.log(err);
          } else {
            console.log(resM.body);
            //Armando respuesta comprobante
            var respuestaBody = {};
            respuestaBody.actualizados = resM.body;
            respuestaBody.cuentaId = idCuenta;
            respuestaBody.movimiento = movimiento.listado;
            res.send(respuestaBody);
          }
        });

      }
    });

  }
  else {
    res.send(400, "Parametros invalidos");
  }

}
);

//Auxiliar para genear los idClientes con la tabla auxiliar del foliador auxfoliador
function generarNuevoIdCliente(callback1, callback2) {

  //Codigo de la funcion principal
  console.log("Ejecutando principal");
  // Ahora que tenemos el contador lo sumamos y asignamos al id
  var claveRegistro = "foliador_cliente_id";
  var idClienteNew = callback1(claveRegistro);

  // Mas codigo de la funcion principal
  console.log("Ejecutando principal before CallBack2", idClienteNew);

  callback2(idClienteNew);
  return idClienteNew;

}


// Funcion para obtener el actual identificador
function obtenerRegistroFoliadorId(claveFoliador) {
  console.log("Ejecutando CALLBACK1");

  // Accedemos ala tabla auxiliar de contadores para obtener el numero de cliente a ingresar
  var id = claveFoliador;
  var query = '&q={"_id": "' + id + '"}&fo=true';

  var clienteMLab = requestjson.createClient(urlAux + query);
  var registro;
  console.log(clienteMLab);
  clienteMLab.get('', function (err, resM, body) {
    if (err) {
      console.log(err);
      return registro;
    } else {
      registro = body;
      console.log("Res body: " + registro);

      return updateRegistroFoliadorId(claveFoliador, registro);
    }
  });

}


// Actualizar el foliador
function updateRegistroFoliadorId(registro) {

  console.log("Ejecutando CALLBACK2", registro._id, registro.value);
  var query = '&q={"_id":"' + registro._id + '"}';
  console.log(" Query = " + query);

  var clienteMLab = requestjson.createClient(urlAux + query);
  // Generar nuevos datos
  var data = { "$set": { "value": + registro.value } };
  console.log(data);

  clienteMLab.put('', data, function (err, res, body) {
    if (err) {
      console.log(err);
      return false;
    } else {
      console.log(res.body, res.statusCode);
      return true;
    }
  });
}

//Auxiliar para genear los folios de operacion con la tabla auxiliar del foliador auxfoliador
function generarNuevoIdOperacion() {

  // Ahora que tenemos el contador lo sumamos y asignamos al id
  var clave = "foliador_operacion_id";
  var registroFoliador = obtenerRegistroFoliadorId(clave);
  console.log(registroFoliador);

  var idOperacionNew = parseInt(registroFoliador.value) + 1;

  // Ahora llamamos al udate con el nuevo cliente generaro
  if (updateRegistroFoliadorId(clave, idOperacionNew));

  return idOperacionNew;

}

// Manejo de session con login.
// Se define la peticion app POST  para generar la session del ususario
app.post("/sessions", function (req, res) {

  console.log("----- POST , accediendo por metodo /sessions ", req.body);
  // Obtener los datos del body.
  var reqCliente = req.body;

  // Recuperar cliente y password de MLab para comparar.
  var query = '&q={"idCliente":' + reqCliente.idCliente + '}&fo=true';
  console.log(" Query = " + query);
  var clienteMLabusuarios = requestjson.createClient(urlUsuarios + query);
  clienteMLabusuarios.get('', function (err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      var userMongo = body;
      if (userMongo) {
        console.log(userMongo.password);
        console.log(reqCliente);

        // Comparar los passwords
        const resultado = bcrypt.compareSync(reqCliente.password, userMongo.password); // true

        if (!resultado) {
          res.statusCode = 401;
          res.send("Autenticacion erronea");
        }
        else {
          // Todo fue correcto, por lo cual mandaremos los datos del cliente con el status OK y se generará el objeto
          // session
          delete userMongo.password;

          req.session.id;
          var sesionOK = {
            token: req.session.id,
            user: userMongo
          };
          console.log("Session OK: ", sesionOK);
          res.send(sesionOK);
        }
      } else {
        res.status(404).send("El cliente no existe");
      }
    }
  });

  console.log(clienteMLabusuarios);
});

//////////////////////////////////////////////
// API para obtener los tipos de operacion soportados por la aplicacion
app.get('/operaciones/', function (req, res) {

  console.log(" ----- GET operaciones");
  var clienteMLabOperaciones = requestjson.createClient(urlOperaciones);
  clienteMLabOperaciones.get('', function (err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      //Envia la peticion al servidor
      res.send(body);
    }
  });

});

//////////////////////////////////////////////////
// API POST para dar de alta una cuenta de un cliente.
app.post("/cuentas", validaSession, function (req, res) {
  //
  console.log("POST cuentas recibido -----");

  // Generar el numero de cuenta
  var idCuenta = obtenerCuentaID();

  // Actualizar el reqbody
  let bodyMongo = req.body;
  bodyMongo.id_cliente = parseInt(bodyMongo.id_cliente);
  bodyMongo._id = parseInt(idCuenta);
  bodyMongo.saldo = 0.00;
  bodyMongo.listado = [];
  bodyMongo.fechaAlta = new Date();

  console.log("To send:", req.body);

  clienteMLab.post('', req.body, function (err, resM, body) {
    if (err) {
      console.log(err);
    } else {
      res.send(body);
    }
  });
});


// Funcion dummy para simular una generacion de cuenta ID
function obtenerCuentaID() {
  return new Date().getFullYear() + "" + (Math.floor(Math.random() * 100)) + "" + idCounter++;
}

# Imagen Base
FROM node:latest

# Directorio de la aplicacion (app)
WORKDIR /app

# Copia de los archivos
# ADD . <-- añade todos los archivos a la carpeta raiz del contenedor /app
ADD .  /app

# Agregar las dependencias
RUN npm install

# Puerto donde se va a abrir el contenedor.
EXPOSE 3000

#Comando con el cual se ejecuta la aplicación
CMD ["npm", "start"]
